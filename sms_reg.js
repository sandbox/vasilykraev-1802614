(function($){

Drupal.behaviors.sms_reg = {
    attach: function (context, settings) {

        var phone_max_length = settings.sms_reg.phone_max_length;
        var phone_mask       = settings.sms_reg.phone_mask;
        // make digit mask for phone nubmer
        $("#edit-number").live('click', function() {
            $(this).mask(phone_mask);
        });

        // hide submit on load
        $("#sms-user-settings-add-form #edit-submit").hide();

        // check for phone number correct
        $("#edit-number").live('keyup', function() {
            // hide submit
            $("#sms-user-settings-add-form #edit-submit").hide();
            var number = $(this).val();
            // remove template _ chars from number, 12345___
            number = number.replace(/[-()_ ]/g, '');
            if (number.length >= phone_max_length) {
                $("#sms-user-settings-add-form #edit-submit").show();
            }
        });

        // you can use js-check for phone verification
        // $("#some-id").bind('click', $.proxy(this.checkUserHasPhone, this));
    },
    /**
     * Checks that current user have phone via ajax
     * @param callback
     */
    checkUserHasPhone: function(callback){
        var that = this;
        //Caching this property to prevent re-requesting
        if(this.hasPhone !== undefined) {
            callback(this.hasPhone);
            if(!this.hasPhone)
                window.location.replace('#overlay=user/' + this.uid + '/edit/mobile');
            return;
        }
        callback = callback || function(){};
        $.ajax({
            url: "sms/checkuser",
            dataType: 'json',
            success:function successCallback(data){
                if (data.status == 'USER_PENDING') {
                    that.hasPhone = false;
                    that.uid = data.uid;
                    window.location.replace('#overlay=user/' + data.uid + '/edit/mobile');
                } else {
                    that.hasPhone = true;
                }
            },
            error:function errorCallback(error){
                that.hasPhone = false;
            },
            complete: function(){
                callback(that.hasPhone);
            }
        });
    }
};

})(jQuery);