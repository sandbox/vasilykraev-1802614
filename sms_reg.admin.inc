<?php

/**
 * @file
 * Administration page callbacks for the SMS registration module.
 */

/*
 * Registration by SMS - admin settings
 */
function sms_reg_settings($form, &$form_state) {
  $data = array();

  $settings = _sms_reg_load_settings();

  $data['phone'] = array(
    '#type' => 'fieldset',
    '#title' => t('Phone settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $data['phone']['phone_max_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Max phone number length'),
    '#size' => 60,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#default_value' => $settings['phone_max_length'],
  );
  $data['phone']['max_sms_per_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('SMS limit per number'),
    '#description' => 'Number of SMS that can be sent to a phone number, before this number will be blocked (added to black list).',
    '#size' => 60,
    '#default_value' => $settings['max_sms_per_phone'],
  );
  $data['phone']['phone_mask'] = array(
    '#title' => t('Phone number mask'),
    '#description' => 'Mask for phone number field using by <a href="http://digitalbush.com/projects/masked-input-plugin/">masked-input-plugin</a>.',
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => $settings['phone_mask'],
  );
  $data['phone']['symbols_remove'] = array(
    '#title' => t('Remove listed symbols from phone number'),
    '#type' => 'checkbox',
    '#default_value' => $settings['symbols_remove'],
);
  $data['phone']['symbols_remove_list'] = array(
    '#description' => 'Remove from phone number unnecessary symbols, like ( ) + - and space.',
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => $settings['symbols_remove_list'],
);

  $data['redirect'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirect users'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $data['redirect']['redirect_unconfirmed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect unverified users to phone verification page.'),
    '#description' => t('User is unconfirmed, if he not yet add phone, or add phone, but not confirm phone number.'),
    '#default_value' => $settings['redirect_unconfirmed'],
  );
  if (module_exists('overlay')) {
    $data['redirect']['redirect_in_overlay'] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect users using overlay module.'),
      '#default_value' => $settings['redirect_in_overlay'],
    );
  }
  $data['redirect']['roles_ignore_redirect'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles, which ignore redirect'),
    '#description' => t('By default - ignore redirect by anonymous & users, which have "administer smsframework" permissions.'),
    '#options' => (user_roles(TRUE)),
    '#default_value' => unserialize($settings['roles_ignore_redirect']),
  );
  $data['redirect']['redirect_type'] = array(
    '#type' => 'radios',
    '#title' => t('Redirect to phone validation by sms from specific pages'),
    '#options' => array(
      'exclude' => t('Enable on every page except the listed pages.'),
      'include' => t('Enable on only the listed pages.'),
    ),
    '#default_value' => $settings['redirect_type'],
  );
  if (user_access('use PHP for block visibility')) {
    $data['redirect']['redirect_type']['#options']['php'] = t('Enable if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
  }
  $data['redirect']['redirect_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => $settings['redirect_pages'],
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>'
    )),
  );

  $form['data'] = $data;
  $form['data']['#tree'] = TRUE;

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults'));

  return $form;
}

function sms_reg_settings_submit($form, &$form_state) {
  $val = $form_state['values'];

  $op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
  if ($op == t('Reset to defaults')) {
    variable_del('sms_reg_settings');
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  else {
    sms_reg_settings_save($form_state);
    drupal_set_message(t('Configuration saved.'));
  }
}

/**
 * Save settings into 'sms_reg_settings' variable
 */
function sms_reg_settings_save($form_state) {
  // Cast strings to integers for properties that need strings
  $props_to_cast = array('phone_max_length', 'max_sms_per_phone');
  foreach ($props_to_cast as $prop) {
    $form_state['values']['data']['phone'][$prop] = (int) $form_state['values']['data']['phone'][$prop];
  }

  $roles_ignore_redirect = array();
  foreach ($form_state['values']['data']['redirect']['roles_ignore_redirect'] as $v) {
    if ($v) $roles_ignore_redirect[] = $v;
  }
  $form_state['values']['data']['redirect']['roles_ignore_redirect'] = serialize($roles_ignore_redirect);

  $form_state['values']['data'] = sms_reg_array_flatten($form_state['values']['data']);
  variable_set('sms_reg_settings', $form_state['values']['data']);
}

/**
 * Flatten an array, preserving its keys.
 */
function sms_reg_array_flatten($array) {
  $result = array();
  if (is_array($array)) {
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $result += sms_reg_array_flatten($value);
      }
      else {
        $result[$key] = $value;
      }
    }
  }
  return $result;
}
