Module: SMS Registration
Author: Vasily Kraev

DESCRIPTION
-------
This module allows users register, login to site & reset the password by using 
they phone number. Module has flexible settings for the phone number and user
redirection depending on user phone number status (not confirmed, pending,
confirmed).

REQUIREMENTS
-------
- SMS Framework module
- SMS Framework user module

INSTALL
-------
1) Install the SMS Framework & SMS user module from http://drupal.org/project/smsframework
2) Drop this module into a subdir of your choice (regarding the Drupal standards)
    for example sites/all/modules
3) Enable the module under admin/build/modules
4) Configure the module under admin/smsframework/registration
5) Use the SMS Registration module or any other module that uses it.